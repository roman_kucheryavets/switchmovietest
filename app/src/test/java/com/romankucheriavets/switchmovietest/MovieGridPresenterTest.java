package com.romankucheriavets.switchmovietest;

import android.content.Context;
import android.test.mock.MockContext;

import com.romankucheriavets.switchmovietest.model.Configuration;
import com.romankucheriavets.switchmovietest.model.Movie;
import com.romankucheriavets.switchmovietest.network.ApiManager;
import com.romankucheriavets.switchmovietest.network.NetworkManager;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieGridPresenter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * Created by mongOose on 19.03.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class MovieGridPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    MockContext context;

    @Mock
    NetworkManager networkManager;

    @Mock
    ApiManager apiManager;

    @Mock
    Configuration configuration;

//    @InjectMocks
    MovieGridPresenter movieGridPresenter;

    @Before
    public void setUp() {
        movieGridPresenter = new MovieGridPresenter(context, networkManager);
    }

    @Test
    public void testGetConfiguration() {
        when(context.getString(any(Integer.class))).thenReturn("ebea8cfca72fdff8d2624ad7bbf78e4c");
        movieGridPresenter.testedString();
//        verify(t).equals("ebea8cfca72fdff8d2624ad7bbf78e4c");
//        when(networkManager.loadConfiguration("ebea8cfca72fdff8d2624ad7bbf78e4c")).thenReturn();
//        movieGridPresenter.startRequestGetConfiguration(2);
//        verify(configuration).getBaseUrl();
    }
}
