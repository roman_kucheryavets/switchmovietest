package com.romankucheriavets.switchmovietest.network;

import com.romankucheriavets.switchmovietest.model.Configuration;
import com.romankucheriavets.switchmovietest.model.Movie;
import com.romankucheriavets.switchmovietest.model.ResponseConfiguration;
import com.romankucheriavets.switchmovietest.model.ResponseNowPlaying;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mongOose on 11.03.2017.
 */

public class NetworkManager {
    private static final String BASE_URL = "http://api.themoviedb.org/";

    private ApiManager apiManager;

    public NetworkManager() {
        apiManager = initApiManager();
    }

    private ApiManager initApiManager() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ApiManager.class);
    }

    public ApiManager getApiManager() {
        return apiManager;
    }

    public Observable<List<Movie>> loadNowPlaying(String key, int page) {
        return apiManager.requestGetNowPlaying(key, page)
                .map(new Function<ResponseNowPlaying, List<Movie>>() {
                    @Override
                    public List<Movie> apply(@NonNull ResponseNowPlaying responseNowPlaying) throws Exception {
                        return responseNowPlaying.getMovies();
                    }
                });
    }

    public Observable<Configuration> loadConfiguration(String key) {
        return apiManager.requestGetConfiguration(key)
                .map(new Function<ResponseConfiguration, Configuration>() {
                    @Override
                    public Configuration apply(@NonNull ResponseConfiguration responseConfiguration) throws Exception {
                        return responseConfiguration.getConfiguration();
                    }
                });
    }

    public Observable<byte[]> loadImage(String url) {
        return apiManager.requestGetImage(url)
                .map(new Function<ResponseBody, byte[]>() {
                    @Override
                    public byte[] apply(@NonNull ResponseBody responseBody) throws Exception {
                        return responseBody.bytes();
                    }
                });
    }
}
