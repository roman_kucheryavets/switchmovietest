package com.romankucheriavets.switchmovietest.network;

import com.romankucheriavets.switchmovietest.model.ResponseNowPlaying;
import com.romankucheriavets.switchmovietest.model.ResponseConfiguration;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by mongOose on 11.03.2017.
 */

public interface ApiManager {

    @GET("3/movie/now_playing?")
    Observable<ResponseNowPlaying> requestGetNowPlaying(@Query("api_key") String key, @Query("page") int page);

    @GET("3/configuration?")
    Observable<ResponseConfiguration> requestGetConfiguration(@Query("api_key") String key);

    @GET
    Observable<ResponseBody> requestGetImage(@Url String url);
}
