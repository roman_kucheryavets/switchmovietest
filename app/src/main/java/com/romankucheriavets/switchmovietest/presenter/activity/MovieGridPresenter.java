package com.romankucheriavets.switchmovietest.presenter.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.romankucheriavets.switchmovietest.R;
import com.romankucheriavets.switchmovietest.model.ResponseConfiguration;
import com.romankucheriavets.switchmovietest.model.Configuration;
import com.romankucheriavets.switchmovietest.model.Movie;
import com.romankucheriavets.switchmovietest.model.ResponseNowPlaying;
import com.romankucheriavets.switchmovietest.network.NetworkManager;
import com.romankucheriavets.switchmovietest.utils.Constants;
import com.romankucheriavets.switchmovietest.utils.Utils;
import com.romankucheriavets.switchmovietest.view.activity.IMovieGridView;
import com.romankucheriavets.switchmovietest.view.activity.MovieDetailsActivity;
import com.romankucheriavets.switchmovietest.view.adapter.binding.MovieGridItemViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * Created by mongOose on 11.03.2017.
 */

public class MovieGridPresenter extends BasePresenter<IMovieGridView> {

    private CompositeDisposable mCompositeDisposable;

    private NetworkManager mNetworkManager;
    private Context mContext;

    private Configuration mConfiguration;
    private ArrayList<Movie> mMovieList = new ArrayList<>();
    private int mPage = 1;
    private int mImageWidth;

    public MovieGridPresenter(Context context, NetworkManager mNetworkManager) {
        this.mContext = context;
        this.mNetworkManager = mNetworkManager;
    }

    public Configuration getConfiguration() {
        return mConfiguration;
    }

    public ArrayList<Movie> getMovieList() {
        return mMovieList;
    }

    public int getPage() {
        return mPage;
    }

    public int getImageWidth() {
        return mImageWidth;
    }

    private Movie findMovieById(ArrayList<Movie> movies, int id) {
        for (Movie movie : movies) {
            if (movie.getId() == id) {
                return movie;
            }
        }
        return null;
    }

    private String buildPosterUrl(String path) {
        return path != null ? (mConfiguration.getBaseUrl()
                + "w" + mImageWidth + path) : Constants.NO_POSTER_URL;
    }

    public String testedString() {
        return mContext.getString(R.string.api_key);
    }

    private int chooseWidth(Configuration configuration, int columnCount) {
        int width = Utils.getScreenWidth();
        List<Integer> integers = new ArrayList<Integer>();
        for (String str : configuration.getPosterSizes()) {
            if (!str.equals("original")) {
                integers.add(Integer.valueOf(str.replace("w", "")));
            }
        }
        return Utils.closest(width / columnCount, integers);
    }

    public void onRestoreState(ArrayList<Movie> movieList, int page, int imageWidth,
                               Configuration configuration) {
        mConfiguration = configuration;
        mMovieList = movieList;
        mImageWidth = imageWidth;
        mPage = page;
    }

    public void startDetailsActivity(int id, Bitmap bitmap) {
        Observable
                .zip(Observable.just(id), Observable.just(bitmap), (integer, bitmap1) -> {
                    Movie movie = new Movie(findMovieById(mMovieList, integer));
                    movie.setImageBytes(Utils.compressBitmap(bitmap1));
                    return movie;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getExtrasForActivity());
    }

    public void startRequestGetConfiguration(final int columnCount) {
        mNetworkManager.loadConfiguration(mContext.getString(R.string.api_key))
                .subscribeOn(Schedulers.io())
                .doOnNext(configuration -> {
                    mConfiguration = configuration;
                    mImageWidth = chooseWidth(mConfiguration, columnCount);
                })
                .subscribe(getConfigurationObserver());
    }

    public void startRequestGetNowPlaying() {
        mNetworkManager.loadNowPlaying(mContext.getString(R.string.api_key), mPage)
                .subscribeOn(Schedulers.io())
                .subscribe(getNowPlayingMovies());
    }

    private void prepareGridItemViewModel(List<Movie> movies) {
        Observable.just(movies)
                .subscribeOn(Schedulers.io())
                .flatMapIterable(movies1 -> movies1)
                .filter(movie -> findMovieById(mMovieList, movie.getId()) != null)
                .flatMap(movie -> {
                    movie.setFullPosterPath(buildPosterUrl(movie.getPosterPath()));
                    return mNetworkManager.loadImage(movie.getFullPosterPath())
                            .zipWith(Observable.just(movie), (bytes, movie1) -> {
                                movie1.setImageBytes(bytes);
                                return movie1;
                            });
                })
                .map(movie -> {
                    byte[] imageBytes = movie.getImageBytes();
                    movie.setImageBytes(null);
                    return new MovieGridItemViewModel(Utils.preparePosterBitmap(
                            imageBytes, 2), movie.getId());
                })
                .toList()
                .toObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getGridViewModels());
    }

    private DisposableObserver<Movie> getExtrasForActivity() {
        return new DisposableObserver<Movie>() {
            @Override
            public void onNext(Movie movie) {
                if (movie != null) {
                    Intent intentDetails = new Intent(mContext, MovieDetailsActivity.class);
                    intentDetails.putExtra(Constants.EXTRA_MOVIE_DATA, movie);
                    mContext.startActivity(intentDetails);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(MovieGridPresenter.this.toString(), e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private DisposableObserver<List<Movie>> getNowPlayingMovies() {
        return new DisposableObserver<List<Movie>>() {
            @Override
            public void onNext(List<Movie> movies) {
                mMovieList.addAll(movies);
                prepareGridItemViewModel(movies);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(MovieGridPresenter.this.toString(), e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private DisposableObserver<List<MovieGridItemViewModel>> getGridViewModels() {
        return new DisposableObserver<List<MovieGridItemViewModel>>() {
            @Override
            public void onNext(List<MovieGridItemViewModel> movieGridItemViewModels) {
                if (isViewAttached()) {
                    mView.onListUpdate(movieGridItemViewModels);
                    mPage++;
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(MovieGridPresenter.this.toString(), e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    mView.onLoadingComplete();
                }
            }
        };
    }

    private DisposableObserver<Configuration> getConfigurationObserver() {
        return new DisposableObserver<Configuration>() {
            @Override
            public void onNext(Configuration responseConfiguration) {
                startRequestGetNowPlaying();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(MovieGridPresenter.this.toString(), e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void onCreate() {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
            mCompositeDisposable.add(getConfigurationObserver());
            mCompositeDisposable.add(getNowPlayingMovies());
            mCompositeDisposable.add(getGridViewModels());
            mCompositeDisposable.add(getExtrasForActivity());
        }
    }

    @Override
    public void onDestroy() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }
}
