package com.romankucheriavets.switchmovietest.presenter.activity;

import android.content.Context;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;

import com.romankucheriavets.switchmovietest.model.Movie;
import com.romankucheriavets.switchmovietest.utils.Utils;
import com.romankucheriavets.switchmovietest.view.activity.IMovieDetailsView;
import com.romankucheriavets.switchmovietest.view.activity.binding.MovieDetailsViewModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by mongOose on 11.03.2017.
 */

public class MovieDetailsPresenter extends BasePresenter<IMovieDetailsView> {

    private CompositeDisposable mCompositeDisposable;

    private Context mContext;

    public MovieDetailsPresenter(Context context) {
        this.mContext = context;
    }

    public void prepareViewModel(Movie movie) {
        Observable.just(movie)
                .subscribeOn(Schedulers.io())
                .map(movie1 -> {
                    MovieDetailsViewModel movieDetailsViewModel = new MovieDetailsViewModel();
                    movieDetailsViewModel.setTitleValue(movie1.getTitle());
                    movieDetailsViewModel.setOverviewValue(movie1.getOverview());
                    movieDetailsViewModel.setReleaseDateValue(Utils.convertDate(movie1.getReleaseDate()));
                    movieDetailsViewModel.setRatingValue(movie1.getAdult());
                    movieDetailsViewModel.setScoreValue(String.valueOf(movie1.getVoteAverage()));
                    movieDetailsViewModel.setBitmapMoviePoster(Utils.preparePosterBitmap(movie1.getImageBytes(), 1));
                    movieDetailsViewModel.setBitmapBlurredBackground(
                            Utils.blurBitmapWithRenderScript(RenderScript.create(mContext),
                                    Utils.changeBitmapContrastBrightness(
                                            Utils.preparePosterBitmap(movie1.getImageBytes(), 1), 0.5f, 0.5f)));
                    return movieDetailsViewModel;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getViewModel());
    }

    private DisposableObserver<MovieDetailsViewModel> getViewModel() {
        return new DisposableObserver<MovieDetailsViewModel>() {
            @Override
            public void onNext(MovieDetailsViewModel movieDetailsViewModel) {
                if (isViewAttached()) {
                    mView.onViewModelReady(movieDetailsViewModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(MovieDetailsPresenter.this.toString(), e.getLocalizedMessage());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void onCreate() {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
            mCompositeDisposable.add(getViewModel());
        }
    }

    @Override
    public void onDestroy() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }
}
