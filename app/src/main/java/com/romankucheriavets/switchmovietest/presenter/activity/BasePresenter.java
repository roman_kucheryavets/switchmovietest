package com.romankucheriavets.switchmovietest.presenter.activity;

/**
 * Created by mongOose on 11.03.2017.
 */

public abstract class BasePresenter<T> {
    T mView;

    public void attachView(T view) {
        this.mView = view;
    }

    public void detachView() {
        if (mView != null) {
            mView = null;
        }
    }

    public boolean isViewAttached() {
        return mView != null;
    }

    public abstract void onCreate();

    public abstract void onDestroy();

    public abstract void onPause();

    public abstract void onResume();
}
