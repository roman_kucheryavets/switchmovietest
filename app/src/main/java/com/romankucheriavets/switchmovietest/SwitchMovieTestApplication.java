package com.romankucheriavets.switchmovietest;

import android.app.Application;
import android.content.Context;

import com.romankucheriavets.switchmovietest.di.component.ApplicationComponent;
import com.romankucheriavets.switchmovietest.di.component.DaggerApplicationComponent;
import com.romankucheriavets.switchmovietest.di.module.ApplicationModule;
import com.romankucheriavets.switchmovietest.di.module.NetworkModule;

/**
 * Created by mongOose on 11.03.2017.
 */

public class SwitchMovieTestApplication extends Application{

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
    }

    public static SwitchMovieTestApplication getApplication(Context context) {
        return (SwitchMovieTestApplication) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    private void initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .networkModule(new NetworkModule())
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
