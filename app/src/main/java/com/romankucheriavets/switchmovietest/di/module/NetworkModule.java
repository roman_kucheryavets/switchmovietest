package com.romankucheriavets.switchmovietest.di.module;

import com.romankucheriavets.switchmovietest.network.NetworkManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mongOose on 11.03.2017.
 */
@Module
public class NetworkModule {

    @Provides
    @Singleton
    NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }
}
