package com.romankucheriavets.switchmovietest.di.component.presenter;

import com.romankucheriavets.switchmovietest.di.module.presenter.MovieDetailsModule;
import com.romankucheriavets.switchmovietest.di.scope.ActivityScope;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieDetailsPresenter;
import com.romankucheriavets.switchmovietest.view.activity.MovieDetailsActivity;

import dagger.Component;

/**
 * Created by mongOose on 11.03.2017.
 */

@ActivityScope
@Component(
        modules = MovieDetailsModule.class
)
public interface MovieDetailsComponent {
    MovieDetailsPresenter movieDetailsActivityPresenter();

    void inject(MovieDetailsActivity movieDetailsActivity);
}
