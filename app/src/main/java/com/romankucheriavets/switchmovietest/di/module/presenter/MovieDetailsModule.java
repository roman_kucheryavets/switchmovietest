package com.romankucheriavets.switchmovietest.di.module.presenter;

import com.romankucheriavets.switchmovietest.di.scope.ActivityScope;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieDetailsPresenter;
import com.romankucheriavets.switchmovietest.view.activity.MovieDetailsActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mongOose on 11.03.2017.
 */

@Module
public class MovieDetailsModule {

    MovieDetailsActivity movieDetailsActivity;

    public MovieDetailsModule(MovieDetailsActivity movieDetailsActivity) {
        this.movieDetailsActivity = movieDetailsActivity;
    }

    @Provides
    @ActivityScope
    MovieDetailsActivity provideMovieDetailsActivity() {
        return movieDetailsActivity;
    }

    @Provides
    @ActivityScope
    MovieDetailsPresenter provideMovieDetailsPresenter() {
        return new MovieDetailsPresenter(movieDetailsActivity);
    }
}
