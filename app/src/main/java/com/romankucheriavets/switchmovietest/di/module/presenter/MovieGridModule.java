package com.romankucheriavets.switchmovietest.di.module.presenter;

import com.romankucheriavets.switchmovietest.di.scope.ActivityScope;
import com.romankucheriavets.switchmovietest.network.NetworkManager;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieGridPresenter;
import com.romankucheriavets.switchmovietest.view.activity.MovieGridActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mongOose on 11.03.2017.
 */
@Module
public class MovieGridModule {

    MovieGridActivity movieGridActivity;

    public MovieGridModule(MovieGridActivity movieGridActivity) {
        this.movieGridActivity = movieGridActivity;
    }

    @Provides
    @ActivityScope
    MovieGridActivity provideMainGridActivity() {
        return movieGridActivity;
    }

    @Provides
    @ActivityScope
    MovieGridPresenter provideMainGridPresenter(NetworkManager networkManager) {
        return new MovieGridPresenter(movieGridActivity, networkManager);
    }
}
