package com.romankucheriavets.switchmovietest.di.scope;

import javax.inject.Scope;

/**
 * Created by mongOose on 11.03.2017.
 */

@Scope
public @interface ActivityScope {
}
