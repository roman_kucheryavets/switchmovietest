package com.romankucheriavets.switchmovietest.di.component.presenter;

import com.romankucheriavets.switchmovietest.di.component.ApplicationComponent;
import com.romankucheriavets.switchmovietest.di.module.presenter.MovieGridModule;
import com.romankucheriavets.switchmovietest.di.scope.ActivityScope;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieGridPresenter;
import com.romankucheriavets.switchmovietest.view.activity.MovieGridActivity;

import dagger.Component;

/**
 * Created by mongOose on 11.03.2017.
 */

@ActivityScope
@Component(
        dependencies = ApplicationComponent.class,
        modules = MovieGridModule.class
)
public interface MovieGridComponent {
    MovieGridPresenter mainGridActivityPresenter();

    void inject(MovieGridActivity movieGridActivity);
}
