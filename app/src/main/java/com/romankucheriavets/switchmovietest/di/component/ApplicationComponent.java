package com.romankucheriavets.switchmovietest.di.component;

import android.app.Application;

import com.romankucheriavets.switchmovietest.di.module.ApplicationModule;
import com.romankucheriavets.switchmovietest.di.module.NetworkModule;
import com.romankucheriavets.switchmovietest.network.NetworkManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by mongOose on 11.03.2017.
 */
@Singleton
@Component(
        modules = {ApplicationModule.class, NetworkModule.class}
)
public interface ApplicationComponent {
    Application application();
    NetworkManager networkManager();
}
