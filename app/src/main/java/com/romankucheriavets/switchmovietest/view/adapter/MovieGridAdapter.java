package com.romankucheriavets.switchmovietest.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.romankucheriavets.switchmovietest.R;
import com.romankucheriavets.switchmovietest.databinding.ActivityMovieGridItemBinding;
import com.romankucheriavets.switchmovietest.view.activity.IMovieGridView;
import com.romankucheriavets.switchmovietest.view.adapter.binding.MovieGridItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mongOose on 11.03.2017.
 */

public class MovieGridAdapter extends RecyclerView.Adapter {

    public static final int VIEW_ITEM = 1;
    public static final int VIEW_PROGRESS = 0;

    private IMovieGridView mActivity;
    private ArrayList<MovieGridItemViewModel> mMovieGridItemViewModels = new ArrayList<>();

    public MovieGridAdapter(IMovieGridView activity) {
        this.mActivity = activity;
    }

    public MovieGridAdapter(IMovieGridView mActivity, ArrayList<MovieGridItemViewModel> mMovieGridItemViewModels) {
        this.mActivity = mActivity;
        this.mMovieGridItemViewModels = mMovieGridItemViewModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == VIEW_ITEM) {
            ActivityMovieGridItemBinding activityMovieGridItemBinding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_movie_grid_item, parent, false);
            View itemView = activityMovieGridItemBinding.getRoot();
            viewHolder = new MovieGridItemViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_movie_grid_progress, parent, false);
            viewHolder = new MovieGridProgressViewHolder(itemView);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieGridItemViewHolder) {
            ((MovieGridItemViewHolder) holder).activityMovieGridItemBinding
                    .setModel(mMovieGridItemViewModels.get(position));
            ((MovieGridItemViewHolder) holder).activityMovieGridItemBinding
                    .setView(mActivity);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mMovieGridItemViewModels.get(position) != null ? VIEW_ITEM : VIEW_PROGRESS;
    }

    @Override
    public int getItemCount() {
        return mMovieGridItemViewModels.size();
    }

    public ArrayList<MovieGridItemViewModel> getMovieGridItems() {
        return mMovieGridItemViewModels;
    }

    public void addMovieGrid(List<MovieGridItemViewModel> movieGridItemViewModels) {
        int offset = mMovieGridItemViewModels.size();
        if (offset > 0) {
            this.mMovieGridItemViewModels.set(offset - 1, movieGridItemViewModels.get(0));
            movieGridItemViewModels.remove(0);
        }
        this.mMovieGridItemViewModels.addAll(movieGridItemViewModels);
        notifyItemRangeInserted(offset, mMovieGridItemViewModels.size() - 1);
    }

    public void addSingleItem(MovieGridItemViewModel movieGridItemViewModel) {
        this.mMovieGridItemViewModels.add(movieGridItemViewModel);
        notifyItemInserted(mMovieGridItemViewModels.size());
    }

    public class MovieGridProgressViewHolder extends RecyclerView.ViewHolder {

        public MovieGridProgressViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class MovieGridItemViewHolder extends RecyclerView.ViewHolder {

        ActivityMovieGridItemBinding activityMovieGridItemBinding;

        public MovieGridItemViewHolder(View itemView) {
            super(itemView);
            activityMovieGridItemBinding = DataBindingUtil.bind(itemView);
        }
    }

}
