package com.romankucheriavets.switchmovietest.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.romankucheriavets.switchmovietest.R;
import com.romankucheriavets.switchmovietest.databinding.ActivityMovieDetailsBinding;
import com.romankucheriavets.switchmovietest.di.component.ApplicationComponent;
import com.romankucheriavets.switchmovietest.di.component.presenter.DaggerMovieDetailsComponent;
import com.romankucheriavets.switchmovietest.di.module.presenter.MovieDetailsModule;
import com.romankucheriavets.switchmovietest.model.Movie;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieDetailsPresenter;
import com.romankucheriavets.switchmovietest.utils.Constants;
import com.romankucheriavets.switchmovietest.view.activity.binding.MovieDetailsViewModel;

public class MovieDetailsActivity extends BaseActivity<MovieDetailsPresenter>
        implements IMovieDetailsView {

    private ActivityMovieDetailsBinding mActivityMovieDetailsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMovieDetailsBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_movie_details);
        Toolbar toolbar = mActivityMovieDetailsBinding.toolbarDetails;
        setSupportActionBar(toolbar);
        setTitle("");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        mPresenter.prepareViewModel(
                (Movie) getIntent().getExtras().getParcelable(Constants.EXTRA_MOVIE_DATA));
    }

    @Override
    protected void initDependencies(ApplicationComponent applicationComponent) {
        DaggerMovieDetailsComponent.builder()
                .movieDetailsModule(new MovieDetailsModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onViewModelReady(MovieDetailsViewModel movieDetailsViewModel) {
        mActivityMovieDetailsBinding.setModel(movieDetailsViewModel);
    }
}
