package com.romankucheriavets.switchmovietest.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.romankucheriavets.switchmovietest.R;

public class TestConstraintLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_constraint_layout);
    }
}
