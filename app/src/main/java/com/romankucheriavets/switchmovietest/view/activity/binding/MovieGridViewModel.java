package com.romankucheriavets.switchmovietest.view.activity.binding;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

/**
 * Created by mongOose on 11.03.2017.
 */

public class MovieGridViewModel {
    public final ObservableBoolean isItemsReady = new ObservableBoolean(false);
    public final ObservableField<String> fieldToolbarGridTitle = new ObservableField<>();
}
