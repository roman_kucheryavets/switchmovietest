package com.romankucheriavets.switchmovietest.view.adapter;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by mongOose on 13.03.2017.
 */

public class BindingAdapters {

    @BindingAdapter("android:setImageFromBitmap")
    public static void setImageButtonSrc(ImageView imageView, Bitmap bitmap) {
        if (bitmap != null) {
            if (imageView.getParent().getParent() instanceof LinearLayout) {
                ((LinearLayout) imageView.getParent().getParent()).setMinimumHeight((int) (bitmap.getWidth() * 1.3));
            }
            imageView.setImageBitmap(bitmap);
        }
    }
}
