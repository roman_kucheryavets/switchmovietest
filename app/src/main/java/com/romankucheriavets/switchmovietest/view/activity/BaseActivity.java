package com.romankucheriavets.switchmovietest.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;

import com.romankucheriavets.switchmovietest.SwitchMovieTestApplication;
import com.romankucheriavets.switchmovietest.di.component.ApplicationComponent;
import com.romankucheriavets.switchmovietest.presenter.activity.BasePresenter;

import javax.inject.Inject;

/**
 * Created by mongOose on 11.03.2017.
 */

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity {

    @Inject
    protected T mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDependencies(SwitchMovieTestApplication.getApplication(this).getApplicationComponent());
        mPresenter.onCreate();
    }

    protected abstract void initDependencies(ApplicationComponent applicationComponent);

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mPresenter.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }
}
