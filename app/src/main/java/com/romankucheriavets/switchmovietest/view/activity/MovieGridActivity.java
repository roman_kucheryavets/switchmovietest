package com.romankucheriavets.switchmovietest.view.activity;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.romankucheriavets.switchmovietest.R;
import com.romankucheriavets.switchmovietest.databinding.ActivityMovieGridBinding;
import com.romankucheriavets.switchmovietest.di.component.ApplicationComponent;
import com.romankucheriavets.switchmovietest.di.component.presenter.DaggerMovieGridComponent;
import com.romankucheriavets.switchmovietest.di.module.presenter.MovieGridModule;
import com.romankucheriavets.switchmovietest.model.Configuration;
import com.romankucheriavets.switchmovietest.model.Movie;
import com.romankucheriavets.switchmovietest.presenter.activity.MovieGridPresenter;
import com.romankucheriavets.switchmovietest.utils.Constants;
import com.romankucheriavets.switchmovietest.utils.Utils;
import com.romankucheriavets.switchmovietest.view.activity.binding.MovieGridViewModel;
import com.romankucheriavets.switchmovietest.view.adapter.MovieGridAdapter;
import com.romankucheriavets.switchmovietest.view.adapter.binding.MovieGridItemViewModel;
import com.romankucheriavets.switchmovietest.view.component.EndlessRecyclerViewOnScrollListener;

import java.util.List;

public class MovieGridActivity extends BaseActivity<MovieGridPresenter> implements IMovieGridView {

    private static final int COLUMN_COUNT_PORTRAIT = 2;
    private static final int COLUMN_COUNT_LANDSCAPE = 4;

    private ActivityMovieGridBinding mActivityMovieGridBinding;
    private MovieGridViewModel mMovieGridViewModel;
    private MovieGridAdapter mMovieGridAdapter;
    private RecyclerView mRecyclerViewMovies;

    private boolean isLoadingComplete = true;
    private int columnCount = COLUMN_COUNT_PORTRAIT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.SwitchMovieTestTheme);
        super.onCreate(savedInstanceState);
        mActivityMovieGridBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_movie_grid);
        mMovieGridViewModel = new MovieGridViewModel();
        mMovieGridViewModel.fieldToolbarGridTitle.set(getString(R.string.activity_movie_grid_title));
        mActivityMovieGridBinding.setView(this);
        mActivityMovieGridBinding.setModel(mMovieGridViewModel);

        Toolbar toolbar = mActivityMovieGridBinding.toolbarGrid;
        setSupportActionBar(toolbar);

        if (Utils.getScreenWidth() > Utils.getScreenHeight()) {
            columnCount = COLUMN_COUNT_LANDSCAPE;
        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, columnCount);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (mMovieGridAdapter.getItemViewType(position)) {
                    case MovieGridAdapter.VIEW_PROGRESS:
                        return columnCount;
                    case MovieGridAdapter.VIEW_ITEM:
                        return 1;
                    default:
                        return -1;
                }
            }
        });
        mRecyclerViewMovies = mActivityMovieGridBinding.contentMovieGrid.recyclerViewNowPlayingMovies;
        mRecyclerViewMovies.setLayoutManager(gridLayoutManager);
        mRecyclerViewMovies.addOnScrollListener(new EndlessRecyclerViewOnScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int totalItemCount) {
                if (isLoadingComplete) {
                    isLoadingComplete = false;
                    mPresenter.startRequestGetNowPlaying();
                    mRecyclerViewMovies.post(() -> onSingleItemUpdate(null));
                }
            }
        });
        mMovieGridAdapter = new MovieGridAdapter(this);
        mRecyclerViewMovies.setAdapter(mMovieGridAdapter);
        if (savedInstanceState != null) {
            isLoadingComplete = savedInstanceState.getBoolean(Constants.BUNDLE_IS_LOADING_COMPLETE);
            mPresenter.onRestoreState(savedInstanceState
                            .<Movie>getParcelableArrayList(Constants.BUNDLE_TO_LOAD_MOVIES_DATA),
                    savedInstanceState.getInt(Constants.BUNDLE_GRID_PAGE),
                    savedInstanceState.getInt(Constants.BUNDLE_GRID_ITEM_WIDTH),
                    (Configuration) savedInstanceState.getParcelable(Constants.BUNDLE_IMAGE_CONFIG));
            if (!isLoadingComplete) {
                mPresenter.startRequestGetNowPlaying();
            }
            mMovieGridAdapter.addMovieGrid(savedInstanceState
                    .<MovieGridItemViewModel>getParcelableArrayList(Constants.BUNDLE_GRID_VIEW_MODELS));
            mRecyclerViewMovies.getLayoutManager().onRestoreInstanceState(
                    savedInstanceState.getParcelable(Constants.BUNDLE_GRID_LAYOUT_MANAGER));
            mMovieGridViewModel.isItemsReady.set(true);
        } else {
            mPresenter.startRequestGetConfiguration(columnCount);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(Constants.BUNDLE_IS_LOADING_COMPLETE, isLoadingComplete);
        outState.putInt(Constants.BUNDLE_GRID_ITEM_WIDTH, mPresenter.getImageWidth());
        outState.putInt(Constants.BUNDLE_GRID_PAGE, mPresenter.getPage());
        outState.putParcelable(Constants.BUNDLE_IMAGE_CONFIG, mPresenter.getConfiguration());
        outState.putParcelableArrayList(Constants.BUNDLE_GRID_VIEW_MODELS,
                mMovieGridAdapter.getMovieGridItems());
        outState.putParcelable(Constants.BUNDLE_GRID_LAYOUT_MANAGER,
                mRecyclerViewMovies.getLayoutManager().onSaveInstanceState());
        outState.putParcelableArrayList(Constants.BUNDLE_TO_LOAD_MOVIES_DATA,
                mPresenter.getMovieList());
    }

    @Override
    protected void initDependencies(ApplicationComponent applicationComponent) {
        DaggerMovieGridComponent.builder()
                .applicationComponent(applicationComponent)
                .movieGridModule(new MovieGridModule(this))
                .build()
                .inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie_grid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_account:
                // No info about this part in task
                Toast.makeText(this, R.string.toast_action_account, Toast.LENGTH_SHORT).show();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int id, Bitmap bitmap) {
        mPresenter.startDetailsActivity(id, bitmap);
    }

    @Override
    public void onLoadMoreInteraction() {

    }

    @Override
    public void onLoadingComplete() {
        isLoadingComplete = true;
    }

    @Override
    public void onListUpdate(List<MovieGridItemViewModel> movieGridItemViewModels) {
        mMovieGridViewModel.isItemsReady.set(true);
        mMovieGridAdapter.addMovieGrid(movieGridItemViewModels);
    }

    @Override
    public void onSingleItemUpdate(MovieGridItemViewModel movieGridItemViewModel) {
        mMovieGridViewModel.isItemsReady.set(true);
        mMovieGridAdapter.addSingleItem(movieGridItemViewModel);
    }

    @Override
    public void onRefreshInteraction() {

    }
}
