package com.romankucheriavets.switchmovietest.view.activity;

import android.graphics.Bitmap;
import android.view.View;

import com.romankucheriavets.switchmovietest.view.adapter.binding.MovieGridItemViewModel;

import java.util.List;

/**
 * Created by mongOose on 11.03.2017.
 */

public interface IMovieGridView {
    void onItemClick(View view, int position, Bitmap bitmap);

    void onLoadMoreInteraction();

    void onLoadingComplete();

    void onListUpdate(List<MovieGridItemViewModel> movieGridItemViewModels);

    void onSingleItemUpdate(MovieGridItemViewModel movieGridItemViewModel);

    void onRefreshInteraction();
}
