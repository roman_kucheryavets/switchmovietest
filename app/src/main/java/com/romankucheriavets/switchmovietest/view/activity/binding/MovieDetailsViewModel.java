package com.romankucheriavets.switchmovietest.view.activity.binding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.graphics.Bitmap;

/**
 * Created by mongOose on 11.03.2017.
 */

public class MovieDetailsViewModel extends BaseObservable{

    private boolean ratingValue;
    private String scoreValue;
    private String releaseDateValue;
    private String titleValue;
    private String overviewValue;

    private Bitmap bitmapBlurredBackground;
    private Bitmap bitmapMoviePoster;

    @Bindable
    public String getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(String scoreValue) {
        this.scoreValue = scoreValue;
    }

    @Bindable
    public boolean getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(boolean ratingValue) {
        this.ratingValue = ratingValue;
    }

    @Bindable
    public String getReleaseDateValue() {
        return releaseDateValue;
    }

    public void setReleaseDateValue(String releaseDateValue) {
        this.releaseDateValue = releaseDateValue;
    }

    @Bindable
    public String getTitleValue() {
        return titleValue;
    }

    public void setTitleValue(String titleValue) {
        this.titleValue = titleValue;
    }

    @Bindable
    public String getOverviewValue() {
        return overviewValue;
    }

    public void setOverviewValue(String overviewValue) {
        this.overviewValue = overviewValue;
    }

    @Bindable
    public Bitmap getBitmapBlurredBackground() {
        return bitmapBlurredBackground;
    }

    public void setBitmapBlurredBackground(Bitmap bitmapBlurredBackground) {
        this.bitmapBlurredBackground = bitmapBlurredBackground;
    }

    @Bindable
    public Bitmap getBitmapMoviePoster() {
        return bitmapMoviePoster;
    }

    public void setBitmapMoviePoster(Bitmap bitmapMoviePoster) {
        this.bitmapMoviePoster = bitmapMoviePoster;
    }
}
