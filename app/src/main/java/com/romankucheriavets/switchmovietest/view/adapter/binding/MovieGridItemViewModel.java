package com.romankucheriavets.switchmovietest.view.adapter.binding;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mongOose on 11.03.2017.
 */

public class MovieGridItemViewModel implements Parcelable {

    private Bitmap posterBitmap;
    private Integer id;

    public MovieGridItemViewModel() {
    }

    public MovieGridItemViewModel(int id) {
        this.id = id;
    }

    public MovieGridItemViewModel(Bitmap posterBitmap, int id) {
        this.posterBitmap = posterBitmap;
        this.id = id;
    }

    protected MovieGridItemViewModel(Parcel in) {
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieGridItemViewModel> CREATOR = new Creator<MovieGridItemViewModel>() {
        @Override
        public MovieGridItemViewModel createFromParcel(Parcel in) {
            return new MovieGridItemViewModel(in);
        }

        @Override
        public MovieGridItemViewModel[] newArray(int size) {
            return new MovieGridItemViewModel[size];
        }
    };

    public Bitmap getPosterBitmap() {
        return posterBitmap;
    }

    public void setPosterBitmap(Bitmap posterBitmap) {
        this.posterBitmap = posterBitmap;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
