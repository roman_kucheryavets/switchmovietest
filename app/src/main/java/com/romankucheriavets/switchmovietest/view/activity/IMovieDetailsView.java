package com.romankucheriavets.switchmovietest.view.activity;

import com.romankucheriavets.switchmovietest.view.activity.binding.MovieDetailsViewModel;

/**
 * Created by mongOose on 13.03.2017.
 */

public interface IMovieDetailsView {
    void onViewModelReady(MovieDetailsViewModel movieDetailsViewModel);
}
