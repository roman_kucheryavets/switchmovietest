package com.romankucheriavets.switchmovietest.utils;

/**
 * Created by mongOose on 14.03.2017.
 */

public class Constants {
    public static final String EXTRA_MOVIE_DATA = "EXTRA_MOVIE_DATA";
    public static final String EXTRA_POSTER_BYTES = "EXTRA_POSTER_BYTES";

    public static final String BUNDLE_GRID_VIEW_MODELS = "BUNDLE_GRID_VIEW_MODELS";
    public static final String BUNDLE_GRID_LAYOUT_MANAGER = "BUNDLE_GRID_LAYOUT_MANAGER";
    public static final String BUNDLE_IS_LOADING_COMPLETE = "BUNDLE_IS_LOADING_COMPLETE";
    public static final String BUNDLE_GRID_ITEM_WIDTH = "BUNDLE_GRID_ITEM_WIDTH";
    public static final String BUNDLE_GRID_PAGE = "BUNDLE_GRID_PAGE";
    public static final String BUNDLE_IMAGE_CONFIG = "BUNDLE_IMAGE_CONFIG";
    public static final String BUNDLE_TO_LOAD_MOVIES_DATA = "BUNDLE_TO_LOAD_MOVIES_DATA";

    public static final String NO_POSTER_URL = "http://www.theprintworks.com/wp-content/themes/psBella/assets/img/film-poster-placeholder.png";

    public static final int ITEMS_PER_PAGE = 20;
}
