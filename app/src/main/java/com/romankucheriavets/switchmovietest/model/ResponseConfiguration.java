package com.romankucheriavets.switchmovietest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mongOose on 12.03.2017.
 */

public class ResponseConfiguration {

    @SerializedName("images")
    @Expose
    private Configuration configuration;
    @SerializedName("change_keys")
    @Expose
    private List<String> changeKeys = null;

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public List<String> getChangeKeys() {
        return changeKeys;
    }

    public void setChangeKeys(List<String> changeKeys) {
        this.changeKeys = changeKeys;
    }
}
